export const config = {
    urlDomain: "https://asistenciawebv2-dev.grupokonecta.co:5005/CustomerVivaAir/VCPE-15/EbVivaAirPeEs/",
    stringTypeChat: 'EbChatVivaAriPeEs',
    urlSkill: "https://widget.grupokonecta.co/genesys/2/chat/allus",
    vqSkill: "vq_vivaperu_chat_espanol",
    switchSkill: "MED_MCR",
    urlLime: "https://limesurvey2.grupokonecta.co:5004/lime_calidad/allusIntegracionChat.php?id_encuesta=53194",
    urlQueue: "https://integracionesbklog.grupokonecta.co:5004/apiwgenesys/api/widget/queue",
    urlTimeServer: "https://asistenciawebv2-dev.grupokonecta.local/api_horario/hora",
    urlGetIp: "https://api.ipify.org?format=jsonp&callback=?",
    dateControlDate: ["24/12/2021", "25/12/2021", "31/12/2021", "01/01/2022"],
    timeControlHour: ["00:00", "23:59", "23:59:00"],
    inter2: null,
    inter3: null,
    interQueue: null,
    intervalTimeC: null,
    booleanWait: false,
    booleanMsgValid: false,
    SessionID: "",
    stringNameUser: "",
    stringNameAgent: "",
    htmlTimeControl:  `<div class='form form-controlhorario' role='form' style='display: block;'>
        <div class="row">
            <div class="col-12 title">
                Te recordamos que nuestro horario de atención es:
            </div>
        </div>
        <div class="row">
            <div class="col-12 controlhorario">
                Horario de atención:<br/>
                Lunes a Viernes 07:30 a 18:00<br/>
                Sábados de 07:30 a 12:00 <br/>
            </div>
        </div>
    </div>`
};