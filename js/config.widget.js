import { config } from './const.widget.js';
import {
    ShowIp, getQueue, validateEmail, custom_menssage, validateDoc, validateName,
    validatePolicies, MessageSystem, playSound, validatePhone, intervalTimeControl, timeControl
} from './function.widget.js';

$(function(){

    if (!window._genesys) window._genesys = {};
    if (!window._gt) window._gt = [];

    /**
    * Arreglo para construccion del widget y las funcionalidades del chat
    *
    * @date 06/12/2021
    * @author Maycol David Sánchez Mora
    */
    window._genesys.widgets =
    {
        main: {
            debug: false,
            theme: "light",
            lang: "es",
            emojis: true,
            actionsMenu: false,
            mobileMode: 'auto',
            mobileModeBreakpoint: 1000,
            i18n: "js/lang.json",
            cookieOptions: {
                secure: true,
                sameSite: 'Strict'
            }
        },
        webchat: {
            apikey: "",
            dataURL: config.urlSkill,
            emojis: false,
            actionsMenu: true,
            uploadsEnabled: false,
            enableCustomHeader: true,
            chatButton: {
                enabled: true,
                template: false,
                openDelay: 1000,
                effectDuration: 300,
                hideDuringInvite: true
            },
            form: {
                wrapper: `
                    <div class='row'>
                        <div class="col col-12 cx-normal mb-20">
                            Preguntas Frecuentes <hr>
                            <p>¡Probablemente encuentres aquí <b>más rápido</b> lo que estas buscando!</p>
                            <p class="aling-right">
                                <a href="https://www.vivaair.com/pe/es/informacion/preguntas-frecuentes">
                                    Ingresa aquí
                                    <img src="`+ config.urlDomain +`img/Ira.png">
                                </a>
                            </p>
                        </div>
                        <div class="col col-12 subtitle">
                            CHAT <hr>
                        </div>
                        <div class="col col-12 mb-20">
                            <span class="wait" id="showQueue"></span>
                        </div>
                    </div>`,
                inputs: [
                    {
                        id: "cx_webchat_form_firstname",
                        name: "firstname",
                        maxlength: "60",
                        placeholder: "Nombre completo",
                        span: "cx-error",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='col col-12 mb-10'>{input}</div>",
                        validate: function (event) {
                            if (event != 0) { return validateName(); }
                        }
                    },
                    {
                        id: "cx_webchat_form_doc",
                        name: "Identify",
                        maxlength: "16",
                        placeholder: "Identificación",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='col col-12 mb-10'>{input}</div>",
                        validate: function (event) {
                            if (event != 0) { return validateDoc(); }
                        }
                    },
                    {
                        id: "cx_webchat_form_email",
                        name: "email",
                        maxlength: "60",
                        placeholder: "Email",
                        type: "Email",
                        span: "cx-error",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='col col-12 mb-10'>{input}</div>",
                        validate: function (event) {
                            if (event != 0) { return validateEmail(); }
                        }
                    },
                    {
                        id: "cx_webchat_form_phone",
                        name: "PhoneNumber",
                        maxlength: "10",
                        placeholder: "Teléfono",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='col col-12 mb-10'>{input}</div>",
                        validate: function (event) {
                            if (event != 0) {
                                return validatePhone();
                            }
                        }
                    },
                    {
                        id: "cx_webchat_form_iporigen",
                        name: "IpOrigen",
                        maxlength: "100",
                        type: "hidden",
                        value: ""
                    },
                    {
                        id: "cx_webchat_form_lastname",
                        name: "lastname",
                        maxlength: "100",
                        placeholder: "lastname",
                        type: "hidden",
                        value: "  "
                    },
                    {
                        id: "cx_webchat_form_subject",
                        name: "subject",
                        maxlength: "100",
                        placeholder: "subject",
                        type: "hidden",
                        value: config.stringTypeChat
                    },
                    {
                        id: "cx_webchat_form_transcripcion",
                        name: "Send_Chat_Transcript",
                        maxlength: "100",
                        placeholder: "transcripcion",
                        type: "hidden",
                        value: "false"
                    },
                    {
                        id: "cx_webchat_form_politicas",
                        name: "sitio",
                        type: "checkbox",
                        span: "cx-error",
                        value: "true",
                        label: `Confirmo conocer y aceptar la 
                            <a href='https://www.vivaair.com/#/pe/es/informacion-legal/politica-privacidad' target='_blank' title='Políticas de privacidad'> política de privacidad</a>`,
                        wrapper: "<div class='col col-12 mb-20 policies'>{input} {label}</div>",
                        validate: function (event) {
                            if (event != 0) { return validatePolicies(event); }
                        }
                    }
                ]
            },
            markdown: false
        }
    };

    if(!window._genesys.widgets.extensions){
        window._genesys.widgets.extensions = {};
    }

    /**
    * Funcion para la creacion de una extencion del widget para poder ejecutar comandos y suscribirse a los eventos
    *
    * @date 06/12/2021
    * @author Maycol David Sánchez Mora
    */
    window._genesys.widgets.extensions["MyPlugin"] = function($){

        var oMyPlugin = window._genesys.widgets.bus.registerPlugin('MyPlugin');

        /**
        * Suscripcion al evento que abre el chat, se valida el control de horario, y se cuarga la ip del cliente
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChat.opened', function(){
            timeControl().then(scheduleValid => {
                if(!scheduleValid.booleanValid){
                    $('.cx-webchat .cx-body').html(config.htmlTimeControl);
                    $('.cx-webchat').addClass('controlHorario');
                } else{
                    ShowIp();
                    intervalTimeControl(scheduleValid.intMiliseg);
                    getQueue();
                    config.interQueue = setInterval(function(){
                        getQueue();
                    }, 10000);
                    /**
                    * Comando para obtener los datos de session del chat
                    *
                    * @date 06/12/2021
                    * @author Maycol David Sánchez Mora
                    */
                    oMyPlugin.command('WebChatService.getSessionData').done(ex => {
                        if(ex.sessionID != "") {
                            oMyPlugin.command('WebChatService.getAgents').done(function(ega) {
                                $.each(ega.agents, function(index, value) {
                                    config.stringNameAgent = value.name;
                                });
                            });
                        }
                    });
                }
            });
        });

        /**
        * Suscripcion al evento de los mensajes recibidos, se valida el control de horario, y se cuarga la ip del cliente
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.messageReceived', function(e){
            if(config.SessionID == ""){ config.SessionID = e.data.sessionData.sessionID; }
            e.data.messages.forEach(element => {
                custom_menssage(element);
            });
        });

        /**
        * Suscripcion al evento de formulario enviado
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChat.submitted', function(){
            clearInterval(config.interQueue);
        });

        /**
        * Suscripcion al evento de chat completado, este evento se ejecuto cuando el chat finaliza y se hubo interacion con el agentes
        * si el chat presenta encuesta en esta opcion se realizaria el llamado de esta
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChat.completed', function(){
            const strMessage = config.urlLime + "&id_interaccion=" + config.SessionID + "&asesor=" + config.stringNameAgent;
            window.open(strMessage);
            setTimeout(()=>{
                oMyPlugin.command('WebChat.close');
            },10000);
        });
        /**
        * Suscripcion al evento de chat finalizado, este evento se ejecuto cuando el chat finaliza independientemente de si hubo o no conexion con el agente
        * si el chat presenta encuesta en esta opcion se realizaria el llamado de esta
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.ended', function(){
            setTimeout(() => {
                oMyPlugin.command('WebChat.close');
            }, 10000);
        });

        /**
        * Comando de chat pre registro de informacion, en este comando se realiza la validacion que el campo de mensajes del chat no envie direcciones web
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.command('WebChatService.registerPreProcessor', { preprocessor: function(message){
            const texto = ""+message.text;
            /**
             * INFORMACIÓN IMPORTANTE
             * Esto es parte de la logica de negocio
             */
            const url = texto.match(/[a-z0-9\-\.]+[^www\.](\.[a-z]{2,4})[a-z0-9\/\-\_\.]*/i);
            /**
             * INFORMACIÓN IMPORTANTE
             * Esto es parte de la logica de negocio
             */
            const email = texto.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

            if (url !== null && email == null) {
                const text_replace = texto.replace(url[0], '').replace('https://', '').replace('http://', '');
                message.text = text_replace;
                $("#cx_input").val(text_replace);
                window._genesys.widgets.common.showAlert($('.cx-widget.cx-webchat'), {text: 'No se permite enviar direciones url al chat', buttonText: 'Ok'});
            } else {
                const text_replace = texto.replace('https://', '').replace('http://', '');
                message.text = text_replace;
            }
            return message;
        }});

        /**
        * Suscripcion al evento de chat cuando un cliente se conecta, en este evento se agregan los mensajes de espera mientras un agente logra conectarse
        *
        * @date 07/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.clientConnected', function(e){
            config.stringNameUser = e.data.message.from.name;
            config.inter3 = setInterval(()=>{
                if(!config.booleanWait){
                    clearInterval(config.inter3);
                    MessageSystem(config.stringNameUser + " estamos ubicando un agente para atender tus inquietudes");
                }
            },20000);

            config.inter2 = setInterval(()=>{
                if(!config.booleanWait && !config.booleanMsgValid){
                    config.booleanMsgValid = true;
                    MessageSystem("Por favor continua en linea. En un momento un asesor te atenderá.");
                }
            },120000);

            config.inter2 = setInterval(()=>{
                if(!config.booleanWait){
                    MessageSystem("Estamos buscando un asesor para atender tu solicitud por favor permanece en línea.");
                }
            },150000);
        });

        /**
        * Suscripcion al evento de chat cuando un agente se conecta, en este evento se agregan los mensajes de espera mientras un agente logra conectarse
        *
        * @date 07/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.agentConnected', function(e){
            playSound();
            clearInterval(config.inter2);
            config.booleanWait= true;
            config.stringNameAgent = e.data.agents.name;
        });

    };

    /**
    * Ajax setup para agregar los encabezados a todas las peticiones
    *
    * @date 07/12/2021
    * @author Maycol David Sánchez Mora
    */
    $.ajaxSetup({
        beforeSend: function (xhr){
           xhr.setRequestHeader("Cache-Control","must-revalidate, no-cache, no-store, private");
           xhr.setRequestHeader("Pragma","no-cache");
        }
    });

    /**
    * Eventos keyup jquery para validar los tipos de datos (Si es numerico solo permitira ingrsar numeros)
    *
    * @date 07/12/2021
    * @author Maycol David Sánchez Mora
    */
    $("body").on('keyup', '#cx_webchat_form_doc', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
    }).on('keyup', '.cx-message-input', function(){
        let texto = $('.cx-message-input').val();
        /**
         * INFORMACIÓN IMPORTANTE
         * Esto es parte de la logica de negocio
         */
        const url = texto.match(/[a-z0-9\-\.]+[^www\.](\.[a-z]{2,4})[a-z0-9\/\-\_\.]*/i);
        /**
         * INFORMACIÓN IMPORTANTE
         * Esto es parte de la logica de negocio
         */
        const email = texto.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

        if (url !== null && email == null) {
            const text_replace = texto.replace(url[0], '').replace('https://', '').replace('http://', '');
            texto = text_replace;
            $("#cx_input").val(text_replace);
            window._genesys.widgets.common.showAlert($('.cx-widget.cx-webchat'), {text: 'No se permite enviar direciones url al chat', buttonText: 'Ok'});
        } else {
            const text_replace = texto.replace('https://', '').replace('http://', '');
            texto = text_replace;
        }

        this.value = texto;
    });

    /**
    * Funcion para la creacion dinamica de los script y css necesarios para el chat
    *
    * @date 07/12/2021
    * @author Maycol David Sánchez Mora
    */
    (function (o) {
        var f = function () {
            var d = o.location;
            o.aTags = o.aTags || [];
            o.aTags.forEach(oTag => {
                var fs = d.getElementsByTagName(oTag.type)[0], e;
                if (!d.getElementById(oTag.id)) {
                    e = d.createElement(oTag.type);
                    e.id = oTag.id;
                }
                if (oTag.type == "script") {
                    e.src = oTag.path;
                }
                else {
                    e.type = 'text/css'; e.rel = 'stylesheet'; e.href = oTag.path;
                }
                if(oTag.integrity){
                    e.integrity = oTag.integrity;
                    e.setAttribute('crossorigin', 'anonymous');
                }
                if (fs) {
                    fs.parentNode.insertBefore(e, fs);
                } else {
                    d.head.appendChild(e);
                }
            });
        }, ol = window.onload;
        if (o.onload) {
            typeof window.onload != "function" ? window.onload = f : window.onload = function () { ol(); f(); };
        }
        else f();
    })
    ({
        location: document,
        onload: false,
        aTags:
            [
                {
                    type: "script",
                    id: "genesys-cx-widget-script",
                    path: config.urlDomain + "js/widgets.min.js",
                    integrity: false
                },
                {
                    type: "link",
                    id: "genesys-cx-widget-styles",
                    path: config.urlDomain + "css/widgets.min.css",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-chats-styles",
                    path: config.urlDomain + "css/webchat.css",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-menu-styles",
                    path: config.urlDomain + "css/widgets.menu.css",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-icons-styles",
                    path: config.urlDomain + "css/icons.css",
                    //integrity: "sha384-+inlLleyeXEc4EjxdEmc3u8vxtUKxZ3RAQly1LbdMbomUpo7b/OJcqBQ5r25+nSO",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-rows-styles",
                    path: config.urlDomain + "css/rows.css",
                    integrity: false
                },
                // {
                //     type: "script",
                //     id: "function-script",
                //     path: config.urlDomain + "js/function.widget.js",
                //     //integrity: "sha384-kMhb2a62f93cRiCw1AxAD+mrzY1Cnqes7A2q76CO7RELATYAnimF4nUBzsGOuKf9",
                //     integrity: false
                // },
                {
                    type: "link",
                    id: "jquery-confirm-styles",
                    path: config.urlDomain + "css/jquery-confirm.min.css",
                    integrity: false
                },
                {
                    type: "script",
                    id: "jquery-confirm-script",
                    path: config.urlDomain + "js/jquery-confirm.min.js",
                    integrity: false
                }
            ]
    });
});