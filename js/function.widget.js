import { config } from './const.widget.js';

/**
* funcion para los mensajes personalidos de espera y la finalizacion del chat por inactividad
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param object element
* @return NULL
*/
export function custom_menssage(element) {
    if(element.type=="Message" && element.from.type=="Agent" ){
        playSound();
    } else if(element.from.type=="Client" && element.type=="Message"){
        config.stringNameUser = element.from.name;
    } else {
        /** Noting */
    }
}

/**
* Funcion para ejecutar el sonido del beep al momento que llega un mensaje
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
export function playSound() {
    var messageSound = new Audio(config.urlDomain + "sound/beep.mp3");
    messageSound.play();
}

/**
* Funcion para la creacion de mensajes de sistemas
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param string stringMsg
*/
export function MessageSystem(stringMsg){
    window._genesys.widgets.bus.command('WebChat.injectMessage', {
        type: 'text',
        text: stringMsg,
        custom: false
    });
}

/**
* Funcion para obtener fecha desde la api horario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return date dateTime
*/
function timeServer(){
    let dateTime;
    $.ajax({
        type: 'POST',
        async: false,
        url: config.urlTimeServer,
        data: null,
        dataType: "json",
        success: function(data){
            dateTime = data;
        }
    });
    return dateTime;
}

/**
* Funcion que define el intervalo el control de horario llamado a la funcion intervalFuntionTimeControl que ejecuta la consulta del control de horario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
export function intervalTimeControl(intMilseg){
    config.intervalTimeC = setInterval(() => {
        intervalFuntionTimeControl();
    }, intMilseg);
}

/**
* Funcion que ejecuta el control de horario llamado desde la funcion de intervalo intervalTimeControl
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return promise
*/
function intervalFuntionTimeControl(){
    return new Promise(resolve => {
        timeControl().then(arraytimeControl => {
            clearInterval(config.intervalTimeC);
            if(!arraytimeControl.booleanValid){
                window._genesys.widgets.bus.command('WebChatService.getSessionData').done(ex => {
                    if(ex.sessionID == "") {
                        $('.cx-webchat .cx-body').html(config.htmlTimeControl);
                        $('.cx-webchat').addClass('controlHorario');
                    } else {
                        MessageSystem("no se pudo asignar un asesor, se finalizara la interacion debido a que estamos fuera de horario");
                        setTimeout(() => {
                            window._genesys.widgets.bus.command('WebChatService.endChat');
                        }, 8000);
                    }
                });
            } else {
                intervalTimeControl(arraytimeControl.intMiliseg);
            }
            resolve();
        });
    });
}

/**
* Funcion para Validar el Control de Horario.
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return promise arraytimeControl
*/
export function timeControl() {
    return new Promise(resolve => {
        var dateTimeServer = timeServer();
        var dateHour = parseInt(dateTimeServer.hour);
        var dateMinutes = parseInt(dateTimeServer.minutes);
        var dateSeconds = parseInt(dateTimeServer.seconds);
        var dateHourActualy = prefix(dateHour)+":"+prefix(dateMinutes)+":"+prefix(dateSeconds);
        var dateDayActualy = prefix(parseInt(dateTimeServer.day))+"/"+prefix(parseInt(dateTimeServer.month))+"/"+prefix(parseInt(dateTimeServer.year));
        var arraytimeControl = {
            booleanValid: true,
            intMiliseg: calculateDifference(dateHourActualy, config.timeControlHour[2])
        };
        config.dateControlDate.forEach(dateNow => {
            if((dateNow == dateDayActualy) && (dateHourActualy < config.timeControlHour[0] || dateHourActualy >= config.timeControlHour[1])){
                arraytimeControl =  {
                    booleanValid: false
                };
            }
        });
        resolve(arraytimeControl);
    });
}

/**
* Funcion para obtener la ip del usuario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
export function ShowIp() {
    setTimeout(() => {
        $(function getip() {
            $.getJSON(config.urlGetIp,
                function(json) {
                    $("#cx_webchat_form_iporigen").val(json.ip);
                }
            );
        });
    }, 500);
}

/**
* Funcion para validar el nombre del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidName
*/
export function validateName() {
    let booleanValidName = false;
    var x = $("#cx_webchat_form_firstname").val();
    if (x == "") {
        alertName(booleanValidName, true);
    } else {
        if (validateTypeText(x)) {
            if(validateLenght("cx_webchat_form_firstname", "alertnamedv", 6, 60)){
                if($('#alertnamedv').length){
                    $('#alertnamedv').remove();
                }
                booleanValidName = true;
            }
        } else {
            alertName(booleanValidName, false);
        }
    }
    return booleanValidName;
}

/**
* Funcion para validar el documento del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidIdentify
*/
export function validateDoc() {
    let booleanValidIdentify = false;
    var x = $("#cx_webchat_form_doc").val();
    if (x == "") {
        alertIndentify(booleanValidIdentify, true);
    } else {
        if (validateTypeNumber(x)) {
            if(validateLenght("cx_webchat_form_doc", "alertidentifydv", 7, 16)){
                if($('#alertidentifydv').length){
                    $('#alertidentifydv').remove();
                }
                booleanValidIdentify = true;
            }
        } else {
            alertIndentify(booleanValidIdentify, false);
        }
    }
    return booleanValidIdentify;
}

/**
* Funcion para validar el documento del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidIdentify
*/
export function validateTypeDoc() {
    let booleanValidIdentify = false;
    var x = $("#cx_webchat_form_type_doc").val();
    if (x == "") {
        alertTypeDoc(booleanValidIdentify);
    } else {
        if($('#alerttypedocdv').length){
            $('#alerttypedocdv').remove();
        }
        booleanValidIdentify = true;
    }
    return booleanValidIdentify;
}

/**
* Funcion para validar el email del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidEmail
*/
export function validateEmail() {
    let booleanValidEmail = false;
    var x = $("#cx_webchat_form_email").val();
    if (x == "") {
        alertEmail(booleanValidEmail, true);
    } else {
        if(!validateTypeEmail(x)){
            alertEmail(booleanValidEmail, false);
        }else{
            if(validateLenght("cx_webchat_form_email", "alertemaildv", 7, 60)){
                if($('#alertemaildv').length){
                    $('#alertemaildv').remove();
                }
                booleanValidEmail = true;
            }
        }
    }
    return booleanValidEmail;
}

/**
* Funcion para validar el numero de telefono del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidPhone
*/
export function validatePhone() {
    let booleanValidPhone = false;
    var x = $("#cx_webchat_form_phone").val();
    if (x == "") {
        alertPhone(booleanValidPhone, true);
    } else {
        if (validateTypeNumber(x)) {
            if(validateLenght("cx_webchat_form_phone", "alertphonedv", 3, 10)){
                if($('#alertphonedv').length){
                    $('#alertphonedv').remove();
                }
                booleanValidPhone = true;
            }
        } else {
            alertPhone(booleanValidPhone, false);
        }
    }
    return booleanValidPhone;
}

/**
* Funcion para validar el numero el aceptamiento de politicas del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidEmail
*/
export function validatePolicies(event) {
    let booleanValidPoliticas = true;
    if (event.type != 'blur' || $.isEmptyObject(event)) {
        const checkedPolicies = $('cx_webchat_form_politicas').is(':checked');
        if(!checkedPolicies){
            window._genesys.widgets.common.showAlert($('.cx-widget.cx-webchat'), {text: 'Para continuar es necesario leer, entender y autorizar la Política de Tratamiento de Datos', buttonText: 'Ok'});
            booleanValidPoliticas = false;
        }
        return booleanValidPoliticas;
    }
}

/**
* Funcion para validar el tamaño de los campos
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: input
* @param string: inputAlert
* @param int: intMin
* @param int: intMax
* @return boolean: booleanValid
*/
function validateLenght(input, inputAlert, intMin, intMax) {
    var x = $("#"+input).val();
    var booleanValid = true;
    if(x.length < intMin || x.length > intMax){
        alertLenght(input, inputAlert, intMin, intMax);
        booleanValid = false;
    }
    return booleanValid;
}

/**
* Funcion para mostrar el mensaje de validacion del campo nombre del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidName
* @param boolean: booleanEmpty
* @return NULL
*/
function alertName(booleanValidName, booleanEmpty){
    if(!booleanValidName){
        if($('#alertnamedv').length){
            $('#alertnamedv').remove();
        }
        const stringMessage = booleanEmpty ? " Ingresa tu nombre" : "Has ingresado un nombre invalido, recuerda que este campo debe contener solo texto.";
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertnamedv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_firstname').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo documento del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidIdentify
* @param boolean: booleanEmpty
* @return NULL
*/
function alertIndentify(booleanValidIdentify, booleanEmpty) {
    if (!booleanValidIdentify) {
        if($('#alertidentifydv').length){
            $('#alertidentifydv').remove();
        }
        const stringMessage = booleanEmpty ? "Ingresa tu identificación" : "Este campo debe ser numérico.";
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertidentifydv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_doc').parent('.col').append(spanName);
    }
}


/**
* Funcion para mostrar el mensaje de validacion del campo tipo de documento del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidTypeDoc
* @return NULL
*/
function alertTypeDoc(booleanValidTypeDoc) {
    if (!booleanValidTypeDoc) {
        if($('#alerttypedocdv').length){
            $('#alerttypedocdv').remove();
        }
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode("Selecione un tipo de documento");
        spanName.style.cssText = 'color:red';
        spanName.id = 'alerttypedocdv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_type_doc').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo email del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidEmail
* @param boolean: booleanEmpty
* @return NULL
*/
function alertEmail(booleanValidEmail, booleanEmpty){
    if(!booleanValidEmail){
        if($('#alertemaildv').length){
            $('#alertemaildv').remove();
        }
        const stringMessage = booleanEmpty ?
        "Ingresa tu correo electrónico, ej:usuario@ejemplo.com" :
        "Has ingresado un correo invalido, revisa la información.";

        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertemaildv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_email').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo telefono del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidPhone
* @param boolean: booleanEmpty
* @return NULL
*/
function alertPhone(booleanValidPhone, booleanEmpty){
    if(!booleanValidPhone){
        if($('#alertphonedv').length){
            $('#alertphonedv').remove();
        }
        const stringMessage = booleanEmpty ?
        "Ingresa tu numero de telefono" :
        "Has ingresado un numero invalido, recuerda que este campo debe ser numérico.";
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode(stringMessage);
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertphonedv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_phone').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion de los tamaños de los campos
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: input
* @param string: inputAlert
* @param int: intMin
* @param int: intMax
* @return NULL
*/
function alertLenght(input, inputAlert, intMin, intMax) {
    if($('#'+inputAlert).length){
        $('#'+inputAlert).remove();
    }
    var spanName = document.createElement("div");
    var contentSpan = document.createTextNode("El campo debe contener entre "+intMin+" y "+intMax+" caracteres");
    spanName.style.cssText = 'color:red';
    spanName.id = inputAlert;
    spanName.classList.add("wc-error");
    spanName.appendChild(contentSpan);

    $('#'+input).parent('.col').append(spanName);
}

/**
* Funcion para validar que un dato sea de tipo numerico
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param int: intNumber
* @return boolean: booleanValid
*/
function validateTypeNumber(intNumber) {
    var checkOK = "0123456789" + "0123456789";
    var checkStr = intNumber;
    var booleanValid = true;

    for (let i = 0; i < checkStr.length; i++) {
        const ch = checkStr.charAt(i);
        let j = 0;
        for (j; j < checkOK.length; j++) {
            if (ch == checkOK.charAt(j))
                break;
        }
        if (j == checkOK.length) {
            booleanValid = false;
            break;
        }
    }

    return booleanValid;
}

/**
* Funcion para validar que un dato sea de tipo string
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: stringText
* @return boolean: booleanValid
*/
function validateTypeText(stringText) {
    var filter = /^[A-Za-z áéíóúñüàè\_\-\.\s\xF1\xD1]+$/;
    var booleanValid = false;
    if (filter.test(stringText)) {
        booleanValid = true;
    }

    return booleanValid;
}

/**
* Funcion para validar que un dato sea de tipo string
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: email
* @return boolean
*/
function validateTypeEmail(email) {
    /**
     * INFORMACIÓN IMPORTANTE
     * Esto es parte de la logica de negocio
     */
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/**
* Funcion para ejecutar el sonido del beep al momento que llega un mensaje
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param int: intNum
* @return string: intNum
*/
function prefix(intNum) {
    return intNum < 10 ? ("0" + intNum) : intNum;
}

/**
* Funcion para calcular la dieferencia entre horas
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param int: intNum
* @return string: intNum
*/
function calculateDifference(dateInitDate, dateEndDate){
    var conversionTable = {
        intSeconds: 1000,
        intMinutes: 60*1000,
        intHours: 60*60*1000
    };

    var convertTime = opts =>
        Object.keys(opts).reduce((end, timeKey) => (
            end + opts[timeKey] * conversionTable[timeKey]
        ), 0);

    var dateInit = new Date("01/01/2000 "+dateInitDate);
    var dateEnd = new Date("01/01/2000 "+dateEndDate);

    var intDiff = dateEnd.getTime() - dateInit.getTime();

    var intMsec = intDiff;
    var intHours = Math.floor(intMsec / 1000 / 60 / 60);
    intMsec -= intHours * 1000 * 60 * 60;
    var intMinutes = Math.floor(intMsec / 1000 / 60);
    intMsec -= intMinutes * 1000 * 60;
    var intSeconds = Math.floor(intMsec / 1000);

    return convertTime({
        intHours: prefix(intHours),
        intMinutes: prefix(intMinutes),
        intSeconds: prefix(intSeconds)
    });

}

/**
* Funcion para obtenerla posicion en cola del chat
*
* @date 01/03/2022
* @author Maycol David Sánchez Mora
* @param null
* @return null
*/
export function getQueue(){
    $.ajax({
        type: "POST",
        async: false,
        url: config.urlQueue,
        data: {
            "vq": config.vqSkill,
            "switch": config.switchSkill
        },
        dataType: "json",
        success: function (response) {
            const posCola = parseInt(response.value) +1;
            const msg = "Estás en la posición de espera: <b>"+posCola+"</b>";
            $('#showQueue').html(msg);
        }
    });
}